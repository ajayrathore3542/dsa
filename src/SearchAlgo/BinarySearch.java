package SearchAlgo;

public class BinarySearch {

	public static int searchEle(int arr[], int item) {
		int n =arr.length;
		int low=0;
		while(low<n) {
			int mid=low+(n-1)/2;
			if(arr[mid]==item) {
				return mid;
			}
			if(arr[mid]<item) {
				low=mid+1;
			}else {
				n=mid-1;
			}
		}
		return -1;
		
	}
	
	
	public static void main(String[] args) {
		
		int arr[] = {2,3,4,5,6,7};
		
		int i =BinarySearch.searchEle(arr, 61);
		if(i==-1) {
			System.out.println("Element not found");
		}else {
			System.out.println("Element is at index : "+ i);
		}
		
	}
}
