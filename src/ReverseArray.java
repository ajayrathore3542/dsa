
public class ReverseArray {

	public static void main(String[] args) {
		int n = 5;

		int b[] = {1,3,4,2,5};
		int len= b.length;
		int temp;
		
		for(int i=0;i<len/2;i++) {
			temp=b[i];
			b[i]=b[len-1-i];
			b[len-1-i]=temp;
			
		}
		
		for(int i=0;i<n;i++) {
			System.out.println(b[i]);
		}
	}
}
